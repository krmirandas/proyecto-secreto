﻿begin;

drop extension if exists pgcrypto;
create extension pgcrypto;

drop schema if exists mexicho cascade;
create schema mexicho;

---------------------------------------------
-- Creamos una tabla de roles de usuarios. --
---------------------------------------------

drop table if exists mexicho.rol;

create table mexicho.rol (
  id serial primary key,
  rol text unique not null
);

comment on table mexicho.rol
is
'El rol ROL que puede tomar un usuario son USUARIO, INFORMADOR, ADMINISTRADOR';

insert into mexicho.rol (id, rol)
values (1, 'USUARIO'),
       (2, 'INFORMADOR'),
       (3, 'ADMINISTRADOR');

drop table if exists mexicho.usuario;

create table mexicho.usuario (
  id serial primary key,
  nombre text unique not null,
  correo text unique not null,
  password text not null,
  fotografia bytea,
  activo boolean not null default false,
  activacion char(32) not null,
  constraint email_valido check (correo ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$')
);

comment on table mexicho.usuario
is
'El usuario NOMBRE tiene el correo CORREO con la contraseña PASSWORD';


---------------------------------------------
-- Un usuario puede tener múltiples roles. --
---------------------------------------------

drop table if exists mexicho.usuario_rol;

create table mexicho.usuario_rol (
  id serial primary key,
  usuario_id integer not null references mexicho.usuario(id) on delete cascade on update cascade,
  rol_id integer not null references mexicho.rol(id) on delete cascade on update cascade
);

-----------------------------------------------------------------------
-- Agregamos un trigger para garantizar que la inserción de usuarios --
-- con su contraseña es segura, es decir, la contraseña del usuario  --
-- se va a cifrar con la función crypt y el algoritmo blowfish 'bf'  --
-- con 8 iteraciones para la función hash.                           --
-----------------------------------------------------------------------

drop function if exists mexicho.hash();

create or replace function mexicho.hash() returns trigger as $$
  begin
    if TG_OP = 'INSERT' then
       if new.activacion is null then
         new.activacion = MD5(new.nombre || new.password || new.correo);
       end if;
       new.password = crypt(new.password, gen_salt('bf', 8)::text);
    end if;
    return new;
  end;
$$ language plpgsql;

comment on function mexicho.hash()
is
'Cifra la contraseña del usuario al guardarla en la base de datos.';

drop trigger if exists hashpassword on mexicho.usuario;

create trigger hashpassword
before insert on mexicho.usuario
for each row execute procedure mexicho.hash();

comment on trigger hashpassword on mexicho.usuario
is
'Hashea la contraseña de un usuario al insertarlo en la base de datos.';

CREATE or replace function mexicho.obten_usuario(usuario text, contraseña text) returns mexicho.usuario as $$
  select *
  from mexicho.usuario
  where usuario = usuario and password = crypt(contraseña, password)
$$ language sql stable;

create or replace function mexicho.obten_rol(usuario text, contraseña text) returns mexicho.usuario as $$
  select *
  from mexicho.usuario
  where usuario = usuario and password = crypt(contraseña, password)
$$ language sql stable;

insert into mexicho.usuario (nombre, password, correo)
values ('Kevin Miranda', '1234', 'kevinmiranda29@ciencias.unam.mx'),
       ('Olga', 'micontraseña', 'olga@mail.com'),
       ('Juan', 'password', 'juan@mail.com');

insert into mexicho.usuario_rol (usuario_id, rol_id)
values (1, 1),
       (1, 3),
       (2, 1),
       (2, 2),
       (3, 1);

create table mexicho.color (
  id serial primary key,
  nombre text not null,
  hex_color text unique not null,
  constraint is_hex_color check (hex_color ~* '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$')
);

insert into mexicho.color (nombre, hex_color)
values ('black', '#000000'),
       ('red', '#FF0000'),
       ('lime', '#00FF00'),
       ('blue', '#0000FF'),
       ('green', '#008000');

create table mexicho.tema (
  id serial primary key,
  color_id integer not null references mexicho.color(id) on delete cascade on update cascade,
  nombre text not null,
  datos text not null
);

insert into mexicho.tema (color_id, nombre, datos)
values (1, 'Campo de futbol', 'Información sobre los campos de futbol conocidos'),
       (2, 'Campo de tenis', 'Información sobre los campos de tenis conocidos'),
       (3, 'Gimnasio al aire libre', 'Información sobre los gimnasios al aire libre'),
       (4, 'Restaurantes mexicanos', 'Restaurantes mexicanos');

create table mexicho.marcador (
  id serial primary key,
  tema_id integer not null references mexicho.tema(id) on delete cascade on update cascade,
  descripcion text not null,
  datos text not null,
  latitud float not null,
  longitud float not null
);

insert into mexicho.marcador (tema_id, descripcion, datos, latitud, longitud)
values (3, 'Gimnasio de casa popular', 'Gimnasio ubicado en la Magdalena Contreras', '19.322930', '-99.221742'),
       (3, 'Gimnasio de alberca olímpica', 'Gimnasio ubicado a un costado de la alberca olímpica de la UNAM', '19.330626', '-99.185229');

create table mexicho.lista_comentario (
  id serial primary key,
  marcador_id integer not null references mexicho.marcador(id) on delete cascade on update cascade
);

insert into mexicho.lista_comentario (marcador_id)
values (1), (2);

create table mexicho.comentario (
  id serial primary key,
  lista_comentario_id integer not null references mexicho.lista_comentario(id) on delete cascade on update cascade,
  comentario text not null,
  calificacion integer not null
);

insert into mexicho.comentario (lista_comentario_id, comentario, calificacion)
values (1, 'Un comentario', 0), (1, 'Dos comentarios', 10),
       (1, 'Tres comentarios', 20), (2, 'Un comentario', 100), (2, 'sin comentarios', 1);


commit;
