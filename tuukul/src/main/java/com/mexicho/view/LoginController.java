package com.mexicho.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.mexicho.model.Usuario;
import com.mexicho.model.UsuarioRol;
import com.mexicho.model.UtilityDB;

/**
 *
 * @author mexicho
 */
@RequestScoped
@ManagedBean
public class LoginController {

    private String usuario;
    private String contraseña;
    private final UtilityDB utility;

    public LoginController() {
        utility = new UtilityDB();
    }

    public String login() {
        Usuario u = utility.obtenUsuario(usuario, contraseña);
        UsuarioRol r = utility.obtenRol(usuario, contraseña);
        if (u != null) {
            if (u.isActivo()) {
                System.out.println("111111111111111111111111111");
               return Pages.INDEX;
            }
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().put("usuario", u);
            System.out.println("22222222222222222222222222222");
            if(r.getId() == 3){
                return Pages.USUARIO;
            } else if(r.getId() == 2){
               return Pages.USUARIO;
            }else{
                return Pages.USUARIO;
            }
        }
        System.out.println("33333333333");
        return Pages.INDEX;
    }

    /**
     * Cierra la sesión http del usuario.
     *
     * @return La página de inicio.
     */
    public String logout() {
        System.out.println("55555555555555555555555555555555555555");
        FacesContext.getCurrentInstance().getExternalContext().
                invalidateSession();

        return Pages.INDEX;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

}
