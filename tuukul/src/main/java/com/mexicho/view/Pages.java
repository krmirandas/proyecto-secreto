/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mexicho.view;

/**
 *
 * @author mexicho
 */
public class Pages {

    public static final String INICIO = "secured/inicio?faces-redirect=true";
    public static final String USUARIO = "secured/perfilUsuario?faces-redirect=true";
    public static final String INDEX = "/index?faces-redirect=true";
    public static final String REGISTRO = "/registro?faces-redirect=true";
    public static final String EROR = "404";

}
