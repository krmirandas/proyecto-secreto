package com.mexicho.view;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.mexicho.model.Tema;
import com.mexicho.model.Color;
import com.mexicho.model.UtilityDB;

@ManagedBean
@RequestScoped
public class TemaController {

    private Tema tema = new Tema();
    private final UtilityDB u = new UtilityDB();
    private String nombre;
    private String datos;
    private Color color;


    public Tema getTema() {
        return tema;
    }

    public void setTema(Tema tema) {
        this.tema = tema;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDatos() {
        return nombre;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void addTema() {
      tema.setNombre(nombre);
      tema.setDatos(datos);
      System.out.println("-------------------------------------------------------------------------------------------------");
      System.out.println(tema);
      tema.setColorId(color);
      u.guardarTema(tema);
      FacesContext.getCurrentInstance()
                .addMessage("registroForm:growl",
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Info", "Se agrego un tema exitosamente"));
    }

}
