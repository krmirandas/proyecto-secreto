package com.mexicho.view;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.mexicho.model.Usuario;
import com.mexicho.model.UtilityDB;

import org.primefaces.model.UploadedFile;

@ManagedBean
@RequestScoped
public class UpdateController {

    private Usuario user = new Usuario();
    private final  UtilityDB utility = new UtilityDB();
    private String confirmacionPassword;
    private UploadedFile fotografia;
    private String usuario;
    private String contraseña;

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public String getConfirmacionPassword() {
        return confirmacionPassword;
    }

    public void setConfirmacionPassword(String confirmacionPassword) {
        this.confirmacionPassword = confirmacionPassword;
    }

    public UploadedFile getFotografia() {
        return fotografia;
    }

    public void setFotografia(UploadedFile fotografia) {
        this.fotografia = fotografia;
    }

    public UpdateController() {
        FacesContext.getCurrentInstance()
                .getViewRoot()
                .setLocale(new Locale("es-Mx"));
    }

    public String updateUser() {
        Usuario u = utility.obtenUsuario(usuario, contraseña);
        if (!u.getPassword().equals(confirmacionPassword)) {
            FacesContext.getCurrentInstance()
                    .addMessage("growl",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Error!", "Fallo de registro: Las contraseñas deben coincidir"));
        } else {
            if (fotografia != null) {
                u.setFotografia(fotografia.getContents());
            } else {
                u.setFotografia(null);
            }
            String nombre = u.getNombre();
            String pass = u.getPassword();
            utility.actualizaUsuario(u);
            FacesContext.getCurrentInstance()
                    .addMessage("growl",
                            new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "Info", "Felicidades, el registro se ha realizado correctamente"));

        }
        return Pages.REGISTRO;
    }

}
